/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_MYSQL_HOME_BUILDBOT_GIT_MKDIST_SQL_SQL_YACC_HH_INCLUDED
# define YY_MYSQL_HOME_BUILDBOT_GIT_MKDIST_SQL_SQL_YACC_HH_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int MYSQLdebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ABORT_SYM = 258,
    ACCESSIBLE_SYM = 259,
    ADD = 260,
    ALL = 261,
    ALTER = 262,
    ANALYZE_SYM = 263,
    AND_AND_SYM = 264,
    AND_SYM = 265,
    AS = 266,
    ASC = 267,
    ASENSITIVE_SYM = 268,
    BEFORE_SYM = 269,
    BETWEEN_SYM = 270,
    BIGINT = 271,
    BINARY = 272,
    BIN_NUM = 273,
    BIT_AND = 274,
    BIT_OR = 275,
    BIT_XOR = 276,
    BLOB_MARIADB_SYM = 277,
    BLOB_ORACLE_SYM = 278,
    BODY_ORACLE_SYM = 279,
    BOTH = 280,
    BY = 281,
    CALL_SYM = 282,
    CASCADE = 283,
    CASE_SYM = 284,
    CAST_SYM = 285,
    CHANGE = 286,
    CHAR_SYM = 287,
    CHECK_SYM = 288,
    COLLATE_SYM = 289,
    CONDITION_SYM = 290,
    CONSTRAINT = 291,
    CONTINUE_MARIADB_SYM = 292,
    CONTINUE_ORACLE_SYM = 293,
    CONVERT_SYM = 294,
    COUNT_SYM = 295,
    CREATE = 296,
    CROSS = 297,
    CUME_DIST_SYM = 298,
    CURDATE = 299,
    CURRENT_USER = 300,
    CURRENT_ROLE = 301,
    CURSOR_SYM = 302,
    CURTIME = 303,
    DATABASE = 304,
    DATABASES = 305,
    DATE_ADD_INTERVAL = 306,
    DATE_SUB_INTERVAL = 307,
    DAY_HOUR_SYM = 308,
    DAY_MICROSECOND_SYM = 309,
    DAY_MINUTE_SYM = 310,
    DAY_SECOND_SYM = 311,
    DECIMAL_NUM = 312,
    DECIMAL_SYM = 313,
    DECLARE_MARIADB_SYM = 314,
    DECLARE_ORACLE_SYM = 315,
    DEFAULT = 316,
    DELETE_DOMAIN_ID_SYM = 317,
    DELETE_SYM = 318,
    DENSE_RANK_SYM = 319,
    DESC = 320,
    DESCRIBE = 321,
    DETERMINISTIC_SYM = 322,
    DISTINCT = 323,
    DIV_SYM = 324,
    DOUBLE_SYM = 325,
    DO_DOMAIN_IDS_SYM = 326,
    DOT_DOT_SYM = 327,
    DROP = 328,
    DUAL_SYM = 329,
    EACH_SYM = 330,
    ELSE = 331,
    ELSEIF_MARIADB_SYM = 332,
    ELSIF_ORACLE_SYM = 333,
    ENCLOSED = 334,
    END_OF_INPUT = 335,
    EQUAL_SYM = 336,
    ESCAPED = 337,
    EXCEPT_SYM = 338,
    EXISTS = 339,
    EXTRACT_SYM = 340,
    FALSE_SYM = 341,
    FETCH_SYM = 342,
    FIRST_VALUE_SYM = 343,
    FLOAT_NUM = 344,
    FLOAT_SYM = 345,
    FORCE_LOOKAHEAD = 346,
    FOREIGN = 347,
    FOR_SYM = 348,
    FOR_SYSTEM_TIME_SYM = 349,
    FROM = 350,
    FULLTEXT_SYM = 351,
    GE = 352,
    GOTO_ORACLE_SYM = 353,
    GRANT = 354,
    GROUP_SYM = 355,
    GROUP_CONCAT_SYM = 356,
    LAG_SYM = 357,
    LEAD_SYM = 358,
    HAVING = 359,
    HEX_NUM = 360,
    HEX_STRING = 361,
    HOUR_MICROSECOND_SYM = 362,
    HOUR_MINUTE_SYM = 363,
    HOUR_SECOND_SYM = 364,
    IDENT = 365,
    IDENT_QUOTED = 366,
    IF_SYM = 367,
    IGNORE_DOMAIN_IDS_SYM = 368,
    IGNORE_SYM = 369,
    INDEX_SYM = 370,
    INFILE = 371,
    INNER_SYM = 372,
    INOUT_SYM = 373,
    INSENSITIVE_SYM = 374,
    INSERT = 375,
    INTERSECT_SYM = 376,
    INTERVAL_SYM = 377,
    INTO = 378,
    INT_SYM = 379,
    IN_SYM = 380,
    IS = 381,
    ITERATE_SYM = 382,
    JOIN_SYM = 383,
    KEYS = 384,
    KEY_SYM = 385,
    KILL_SYM = 386,
    LE = 387,
    LEADING = 388,
    LEAVE_SYM = 389,
    LEFT = 390,
    LEX_HOSTNAME = 391,
    LIKE = 392,
    LIMIT = 393,
    LINEAR_SYM = 394,
    LINES = 395,
    LOAD = 396,
    LOCATOR_SYM = 397,
    LOCK_SYM = 398,
    LONGBLOB = 399,
    LONGTEXT = 400,
    LONG_NUM = 401,
    LONG_SYM = 402,
    LOOP_SYM = 403,
    LOW_PRIORITY = 404,
    MASTER_SSL_VERIFY_SERVER_CERT_SYM = 405,
    MATCH = 406,
    MAX_SYM = 407,
    MAXVALUE_SYM = 408,
    MEDIAN_SYM = 409,
    MEDIUMBLOB = 410,
    MEDIUMINT = 411,
    MEDIUMTEXT = 412,
    MINUTE_MICROSECOND_SYM = 413,
    MINUTE_SECOND_SYM = 414,
    MIN_SYM = 415,
    MODIFIES_SYM = 416,
    MOD_SYM = 417,
    MYSQL_CONCAT_SYM = 418,
    NATURAL = 419,
    NCHAR_STRING = 420,
    NE = 421,
    NEG = 422,
    NOT2_SYM = 423,
    NOT_SYM = 424,
    NOW_SYM = 425,
    NO_WRITE_TO_BINLOG = 426,
    NTILE_SYM = 427,
    NULL_SYM = 428,
    NUM = 429,
    NUMERIC_SYM = 430,
    NTH_VALUE_SYM = 431,
    ON = 432,
    OPTIMIZE = 433,
    OPTIONALLY = 434,
    ORACLE_CONCAT_SYM = 435,
    OR2_SYM = 436,
    ORDER_SYM = 437,
    OR_SYM = 438,
    OTHERS_ORACLE_SYM = 439,
    OUTER = 440,
    OUTFILE = 441,
    OUT_SYM = 442,
    OVER_SYM = 443,
    PACKAGE_ORACLE_SYM = 444,
    PAGE_CHECKSUM_SYM = 445,
    PARAM_MARKER = 446,
    PARSE_VCOL_EXPR_SYM = 447,
    PARTITION_SYM = 448,
    PERCENT_ORACLE_SYM = 449,
    PERCENT_RANK_SYM = 450,
    PERCENTILE_CONT_SYM = 451,
    PERCENTILE_DISC_SYM = 452,
    POSITION_SYM = 453,
    PRECISION = 454,
    PRIMARY_SYM = 455,
    PROCEDURE_SYM = 456,
    PURGE = 457,
    RAISE_ORACLE_SYM = 458,
    RANGE_SYM = 459,
    RANK_SYM = 460,
    READS_SYM = 461,
    READ_SYM = 462,
    READ_WRITE_SYM = 463,
    REAL = 464,
    RECURSIVE_SYM = 465,
    REF_SYSTEM_ID_SYM = 466,
    REFERENCES = 467,
    REGEXP = 468,
    RELEASE_SYM = 469,
    RENAME = 470,
    REPEAT_SYM = 471,
    REPLACE = 472,
    REQUIRE_SYM = 473,
    RESIGNAL_SYM = 474,
    RESTRICT = 475,
    RETURNING_SYM = 476,
    RETURN_MARIADB_SYM = 477,
    RETURN_ORACLE_SYM = 478,
    REVOKE = 479,
    RIGHT = 480,
    ROWS_SYM = 481,
    ROWTYPE_ORACLE_SYM = 482,
    ROW_NUMBER_SYM = 483,
    SECOND_MICROSECOND_SYM = 484,
    SELECT_SYM = 485,
    SENSITIVE_SYM = 486,
    SEPARATOR_SYM = 487,
    SERVER_OPTIONS = 488,
    SET = 489,
    SET_VAR = 490,
    SHIFT_LEFT = 491,
    SHIFT_RIGHT = 492,
    SHOW = 493,
    SIGNAL_SYM = 494,
    SMALLINT = 495,
    SPATIAL_SYM = 496,
    SPECIFIC_SYM = 497,
    SQLEXCEPTION_SYM = 498,
    SQLSTATE_SYM = 499,
    SQLWARNING_SYM = 500,
    SQL_BIG_RESULT = 501,
    SQL_SMALL_RESULT = 502,
    SQL_SYM = 503,
    SSL_SYM = 504,
    STARTING = 505,
    STATS_AUTO_RECALC_SYM = 506,
    STATS_PERSISTENT_SYM = 507,
    STATS_SAMPLE_PAGES_SYM = 508,
    STDDEV_SAMP_SYM = 509,
    STD_SYM = 510,
    STRAIGHT_JOIN = 511,
    SUBSTRING = 512,
    SUM_SYM = 513,
    SYSDATE = 514,
    TABLE_REF_PRIORITY = 515,
    TABLE_SYM = 516,
    TERMINATED = 517,
    TEXT_STRING = 518,
    THEN_SYM = 519,
    TINYBLOB = 520,
    TINYINT = 521,
    TINYTEXT = 522,
    TO_SYM = 523,
    TRAILING = 524,
    TRIGGER_SYM = 525,
    TRIM = 526,
    TRUE_SYM = 527,
    ULONGLONG_NUM = 528,
    UNDERSCORE_CHARSET = 529,
    UNDO_SYM = 530,
    UNION_SYM = 531,
    UNIQUE_SYM = 532,
    UNLOCK_SYM = 533,
    UNSIGNED = 534,
    UPDATE_SYM = 535,
    USAGE = 536,
    USE_SYM = 537,
    USING = 538,
    UTC_DATE_SYM = 539,
    UTC_TIMESTAMP_SYM = 540,
    UTC_TIME_SYM = 541,
    VALUES = 542,
    VALUES_IN_SYM = 543,
    VALUES_LESS_SYM = 544,
    VARBINARY = 545,
    VARCHAR = 546,
    VARIANCE_SYM = 547,
    VARYING = 548,
    VAR_SAMP_SYM = 549,
    WHEN_SYM = 550,
    WHERE = 551,
    WHILE_SYM = 552,
    WITH = 553,
    WITH_CUBE_SYM = 554,
    WITH_ROLLUP_SYM = 555,
    WITH_SYSTEM_SYM = 556,
    XOR = 557,
    YEAR_MONTH_SYM = 558,
    ZEROFILL = 559,
    IMPOSSIBLE_ACTION = 560,
    BODY_MARIADB_SYM = 561,
    ELSEIF_ORACLE_SYM = 562,
    ELSIF_MARIADB_SYM = 563,
    EXCEPTION_ORACLE_SYM = 564,
    GOTO_MARIADB_SYM = 565,
    OTHERS_MARIADB_SYM = 566,
    PACKAGE_MARIADB_SYM = 567,
    RAISE_MARIADB_SYM = 568,
    ROWTYPE_MARIADB_SYM = 569,
    ACTION = 570,
    ADMIN_SYM = 571,
    ADDDATE_SYM = 572,
    AFTER_SYM = 573,
    AGAINST = 574,
    AGGREGATE_SYM = 575,
    ALGORITHM_SYM = 576,
    ALWAYS_SYM = 577,
    ANY_SYM = 578,
    ASCII_SYM = 579,
    AT_SYM = 580,
    ATOMIC_SYM = 581,
    AUTHORS_SYM = 582,
    AUTOEXTEND_SIZE_SYM = 583,
    AUTO_INC = 584,
    AUTO_SYM = 585,
    AVG_ROW_LENGTH = 586,
    AVG_SYM = 587,
    BACKUP_SYM = 588,
    BEGIN_MARIADB_SYM = 589,
    BEGIN_ORACLE_SYM = 590,
    BINLOG_SYM = 591,
    BIT_SYM = 592,
    BLOCK_SYM = 593,
    BOOL_SYM = 594,
    BOOLEAN_SYM = 595,
    BTREE_SYM = 596,
    BYTE_SYM = 597,
    CACHE_SYM = 598,
    CASCADED = 599,
    CATALOG_NAME_SYM = 600,
    CHAIN_SYM = 601,
    CHANGED = 602,
    CHARSET = 603,
    CHECKPOINT_SYM = 604,
    CHECKSUM_SYM = 605,
    CIPHER_SYM = 606,
    CLASS_ORIGIN_SYM = 607,
    CLIENT_SYM = 608,
    CLOB_MARIADB_SYM = 609,
    CLOB_ORACLE_SYM = 610,
    CLOSE_SYM = 611,
    COALESCE = 612,
    CODE_SYM = 613,
    COLLATION_SYM = 614,
    COLON_ORACLE_SYM = 615,
    COLUMNS = 616,
    COLUMN_ADD_SYM = 617,
    COLUMN_CHECK_SYM = 618,
    COLUMN_CREATE_SYM = 619,
    COLUMN_DELETE_SYM = 620,
    COLUMN_GET_SYM = 621,
    COLUMN_SYM = 622,
    COLUMN_NAME_SYM = 623,
    COMMENT_SYM = 624,
    COMMITTED_SYM = 625,
    COMMIT_SYM = 626,
    COMPACT_SYM = 627,
    COMPLETION_SYM = 628,
    COMPRESSED_SYM = 629,
    CONCURRENT = 630,
    CONNECTION_SYM = 631,
    CONSISTENT_SYM = 632,
    CONSTRAINT_CATALOG_SYM = 633,
    CONSTRAINT_NAME_SYM = 634,
    CONSTRAINT_SCHEMA_SYM = 635,
    CONTAINS_SYM = 636,
    CONTEXT_SYM = 637,
    CONTRIBUTORS_SYM = 638,
    CPU_SYM = 639,
    CUBE_SYM = 640,
    CURRENT_SYM = 641,
    CURRENT_POS_SYM = 642,
    CURSOR_NAME_SYM = 643,
    CYCLE_SYM = 644,
    DATAFILE_SYM = 645,
    DATA_SYM = 646,
    DATETIME = 647,
    DATE_FORMAT_SYM = 648,
    DATE_SYM = 649,
    DAY_SYM = 650,
    DEALLOCATE_SYM = 651,
    DECODE_MARIADB_SYM = 652,
    DECODE_ORACLE_SYM = 653,
    DEFINER_SYM = 654,
    DELAYED_SYM = 655,
    DELAY_KEY_WRITE_SYM = 656,
    DES_KEY_FILE = 657,
    DIAGNOSTICS_SYM = 658,
    DIRECTORY_SYM = 659,
    DISABLE_SYM = 660,
    DISCARD = 661,
    DISK_SYM = 662,
    DO_SYM = 663,
    DUMPFILE = 664,
    DUPLICATE_SYM = 665,
    DYNAMIC_SYM = 666,
    ENABLE_SYM = 667,
    END = 668,
    ENDS_SYM = 669,
    ENGINES_SYM = 670,
    ENGINE_SYM = 671,
    ENUM = 672,
    ERROR_SYM = 673,
    ERRORS = 674,
    ESCAPE_SYM = 675,
    EVENTS_SYM = 676,
    EVENT_SYM = 677,
    EVERY_SYM = 678,
    EXCHANGE_SYM = 679,
    EXAMINED_SYM = 680,
    EXCLUDE_SYM = 681,
    EXECUTE_SYM = 682,
    EXCEPTION_MARIADB_SYM = 683,
    EXIT_MARIADB_SYM = 684,
    EXIT_ORACLE_SYM = 685,
    EXPANSION_SYM = 686,
    EXPORT_SYM = 687,
    EXTENDED_SYM = 688,
    EXTENT_SIZE_SYM = 689,
    FAST_SYM = 690,
    FAULTS_SYM = 691,
    FILE_SYM = 692,
    FIRST_SYM = 693,
    FIXED_SYM = 694,
    FLUSH_SYM = 695,
    FOLLOWS_SYM = 696,
    FOLLOWING_SYM = 697,
    FORCE_SYM = 698,
    FORMAT_SYM = 699,
    FOUND_SYM = 700,
    FULL = 701,
    FUNCTION_SYM = 702,
    GENERAL = 703,
    GENERATED_SYM = 704,
    GEOMETRYCOLLECTION = 705,
    GEOMETRY_SYM = 706,
    GET_FORMAT = 707,
    GET_SYM = 708,
    GLOBAL_SYM = 709,
    GRANTS = 710,
    HANDLER_SYM = 711,
    HARD_SYM = 712,
    HASH_SYM = 713,
    HELP_SYM = 714,
    HIGH_PRIORITY = 715,
    HISTORY_SYM = 716,
    HOST_SYM = 717,
    HOSTS_SYM = 718,
    HOUR_SYM = 719,
    ID_SYM = 720,
    IDENTIFIED_SYM = 721,
    IGNORE_SERVER_IDS_SYM = 722,
    IMMEDIATE_SYM = 723,
    IMPORT = 724,
    INCREMENT_SYM = 725,
    INDEXES = 726,
    INITIAL_SIZE_SYM = 727,
    INSERT_METHOD = 728,
    INSTALL_SYM = 729,
    INVOKER_SYM = 730,
    IO_SYM = 731,
    IPC_SYM = 732,
    ISOLATION = 733,
    ISOPEN_SYM = 734,
    ISSUER_SYM = 735,
    INVISIBLE_SYM = 736,
    JSON_SYM = 737,
    KEY_BLOCK_SIZE = 738,
    LANGUAGE_SYM = 739,
    LAST_SYM = 740,
    LAST_VALUE = 741,
    LASTVAL_SYM = 742,
    LEAVES = 743,
    LESS_SYM = 744,
    LEVEL_SYM = 745,
    LINESTRING = 746,
    LIST_SYM = 747,
    LOCAL_SYM = 748,
    LOCKS_SYM = 749,
    LOGFILE_SYM = 750,
    LOGS_SYM = 751,
    MASTER_CONNECT_RETRY_SYM = 752,
    MASTER_DELAY_SYM = 753,
    MASTER_GTID_POS_SYM = 754,
    MASTER_HOST_SYM = 755,
    MASTER_LOG_FILE_SYM = 756,
    MASTER_LOG_POS_SYM = 757,
    MASTER_PASSWORD_SYM = 758,
    MASTER_PORT_SYM = 759,
    MASTER_SERVER_ID_SYM = 760,
    MASTER_SSL_CAPATH_SYM = 761,
    MASTER_SSL_CA_SYM = 762,
    MASTER_SSL_CERT_SYM = 763,
    MASTER_SSL_CIPHER_SYM = 764,
    MASTER_SSL_CRL_SYM = 765,
    MASTER_SSL_CRLPATH_SYM = 766,
    MASTER_SSL_KEY_SYM = 767,
    MASTER_SSL_SYM = 768,
    MASTER_SYM = 769,
    MASTER_USER_SYM = 770,
    MASTER_USE_GTID_SYM = 771,
    MASTER_HEARTBEAT_PERIOD_SYM = 772,
    MAX_CONNECTIONS_PER_HOUR = 773,
    MAX_QUERIES_PER_HOUR = 774,
    MAX_ROWS = 775,
    MAX_SIZE_SYM = 776,
    MAX_UPDATES_PER_HOUR = 777,
    MAX_STATEMENT_TIME_SYM = 778,
    MAX_USER_CONNECTIONS_SYM = 779,
    MEDIUM_SYM = 780,
    MEMORY_SYM = 781,
    MERGE_SYM = 782,
    MESSAGE_TEXT_SYM = 783,
    MICROSECOND_SYM = 784,
    MIGRATE_SYM = 785,
    MINUTE_SYM = 786,
    MINVALUE_SYM = 787,
    MIN_ROWS = 788,
    MODE_SYM = 789,
    MODIFY_SYM = 790,
    MONTH_SYM = 791,
    MULTILINESTRING = 792,
    MULTIPOINT = 793,
    MULTIPOLYGON = 794,
    MUTEX_SYM = 795,
    MYSQL_SYM = 796,
    MYSQL_ERRNO_SYM = 797,
    NAMES_SYM = 798,
    NAME_SYM = 799,
    NATIONAL_SYM = 800,
    NCHAR_SYM = 801,
    NEW_SYM = 802,
    NEXT_SYM = 803,
    NEXTVAL_SYM = 804,
    NOCACHE_SYM = 805,
    NOCYCLE_SYM = 806,
    NODEGROUP_SYM = 807,
    NONE_SYM = 808,
    NOTFOUND_SYM = 809,
    NO_SYM = 810,
    NOMAXVALUE_SYM = 811,
    NOMINVALUE_SYM = 812,
    NO_WAIT_SYM = 813,
    NOWAIT_SYM = 814,
    NUMBER_MARIADB_SYM = 815,
    NUMBER_ORACLE_SYM = 816,
    NVARCHAR_SYM = 817,
    OF_SYM = 818,
    OFFSET_SYM = 819,
    OLD_PASSWORD_SYM = 820,
    ONE_SYM = 821,
    ONLY_SYM = 822,
    ONLINE_SYM = 823,
    OPEN_SYM = 824,
    OPTIONS_SYM = 825,
    OPTION = 826,
    OWNER_SYM = 827,
    PACK_KEYS_SYM = 828,
    PAGE_SYM = 829,
    PARSER_SYM = 830,
    PARTIAL = 831,
    PARTITIONS_SYM = 832,
    PARTITIONING_SYM = 833,
    PASSWORD_SYM = 834,
    PERIOD_SYM = 835,
    PERSISTENT_SYM = 836,
    PHASE_SYM = 837,
    PLUGINS_SYM = 838,
    PLUGIN_SYM = 839,
    POINT_SYM = 840,
    POLYGON = 841,
    PORT_SYM = 842,
    PRECEDES_SYM = 843,
    PRECEDING_SYM = 844,
    PREPARE_SYM = 845,
    PRESERVE_SYM = 846,
    PREV_SYM = 847,
    PREVIOUS_SYM = 848,
    PRIVILEGES = 849,
    PROCESS = 850,
    PROCESSLIST_SYM = 851,
    PROFILE_SYM = 852,
    PROFILES_SYM = 853,
    PROXY_SYM = 854,
    QUARTER_SYM = 855,
    QUERY_SYM = 856,
    QUICK = 857,
    RAW_MARIADB_SYM = 858,
    RAW_ORACLE_SYM = 859,
    READ_ONLY_SYM = 860,
    REBUILD_SYM = 861,
    RECOVER_SYM = 862,
    REDOFILE_SYM = 863,
    REDO_BUFFER_SIZE_SYM = 864,
    REDUNDANT_SYM = 865,
    RELAY = 866,
    RELAYLOG_SYM = 867,
    RELAY_LOG_FILE_SYM = 868,
    RELAY_LOG_POS_SYM = 869,
    RELAY_THREAD = 870,
    RELOAD = 871,
    REMOVE_SYM = 872,
    REORGANIZE_SYM = 873,
    REPAIR = 874,
    REPEATABLE_SYM = 875,
    REPLICATION = 876,
    RESET_SYM = 877,
    RESTART_SYM = 878,
    RESOURCES = 879,
    RESTORE_SYM = 880,
    RESUME_SYM = 881,
    RETURNED_SQLSTATE_SYM = 882,
    RETURNS_SYM = 883,
    REUSE_SYM = 884,
    REVERSE_SYM = 885,
    ROLE_SYM = 886,
    ROLLBACK_SYM = 887,
    ROLLUP_SYM = 888,
    ROUTINE_SYM = 889,
    ROWCOUNT_SYM = 890,
    ROW_SYM = 891,
    ROW_COUNT_SYM = 892,
    ROW_FORMAT_SYM = 893,
    RTREE_SYM = 894,
    SAVEPOINT_SYM = 895,
    SCHEDULE_SYM = 896,
    SCHEMA_NAME_SYM = 897,
    SECOND_SYM = 898,
    SECURITY_SYM = 899,
    SEQUENCE_SYM = 900,
    SERIALIZABLE_SYM = 901,
    SERIAL_SYM = 902,
    SESSION_SYM = 903,
    SERVER_SYM = 904,
    SETVAL_SYM = 905,
    SHARE_SYM = 906,
    SHUTDOWN = 907,
    SIGNED_SYM = 908,
    SIMPLE_SYM = 909,
    SLAVE = 910,
    SLAVES = 911,
    SLAVE_POS_SYM = 912,
    SLOW = 913,
    SNAPSHOT_SYM = 914,
    SOCKET_SYM = 915,
    SOFT_SYM = 916,
    SONAME_SYM = 917,
    SOUNDS_SYM = 918,
    SOURCE_SYM = 919,
    SQL_BUFFER_RESULT = 920,
    SQL_CACHE_SYM = 921,
    SQL_CALC_FOUND_ROWS = 922,
    SQL_NO_CACHE_SYM = 923,
    SQL_THREAD = 924,
    STARTS_SYM = 925,
    START_SYM = 926,
    STATEMENT_SYM = 927,
    STATUS_SYM = 928,
    STOP_SYM = 929,
    STORAGE_SYM = 930,
    STORED_SYM = 931,
    STRING_SYM = 932,
    SUBCLASS_ORIGIN_SYM = 933,
    SUBDATE_SYM = 934,
    SUBJECT_SYM = 935,
    SUBPARTITIONS_SYM = 936,
    SUBPARTITION_SYM = 937,
    SUPER_SYM = 938,
    SUSPEND_SYM = 939,
    SWAPS_SYM = 940,
    SWITCHES_SYM = 941,
    SYSTEM = 942,
    SYSTEM_TIME_SYM = 943,
    TABLES = 944,
    TABLESPACE = 945,
    TABLE_CHECKSUM_SYM = 946,
    TABLE_NAME_SYM = 947,
    TEMPORARY = 948,
    TEMPTABLE_SYM = 949,
    TEXT_SYM = 950,
    THAN_SYM = 951,
    TIES_SYM = 952,
    TIMESTAMP = 953,
    TIMESTAMP_ADD = 954,
    TIMESTAMP_DIFF = 955,
    TIME_SYM = 956,
    TRANSACTION_SYM = 957,
    TRANSACTIONAL_SYM = 958,
    TRIGGERS_SYM = 959,
    TRIM_ORACLE = 960,
    TRUNCATE_SYM = 961,
    TYPES_SYM = 962,
    TYPE_SYM = 963,
    UDF_RETURNS_SYM = 964,
    UNBOUNDED_SYM = 965,
    UNCOMMITTED_SYM = 966,
    UNDEFINED_SYM = 967,
    UNDOFILE_SYM = 968,
    UNDO_BUFFER_SIZE_SYM = 969,
    UNICODE_SYM = 970,
    UNINSTALL_SYM = 971,
    UNKNOWN_SYM = 972,
    UNTIL_SYM = 973,
    UPGRADE_SYM = 974,
    USER_SYM = 975,
    USE_FRM = 976,
    VALUE_SYM = 977,
    VARCHAR2_MARIADB_SYM = 978,
    VARCHAR2_ORACLE_SYM = 979,
    VARIABLES = 980,
    VERSIONING_SYM = 981,
    VIA_SYM = 982,
    VIEW_SYM = 983,
    VIRTUAL_SYM = 984,
    WAIT_SYM = 985,
    WARNINGS = 986,
    WEEK_SYM = 987,
    WEIGHT_STRING_SYM = 988,
    WINDOW_SYM = 989,
    WITHIN = 990,
    WITHOUT = 991,
    WORK_SYM = 992,
    WRAPPER_SYM = 993,
    WRITE_SYM = 994,
    X509_SYM = 995,
    XA_SYM = 996,
    XML_SYM = 997,
    YEAR_SYM = 998,
    CONDITIONLESS_JOIN = 999,
    ON_SYM = 1000,
    PREC_BELOW_NOT = 1001,
    PREC_BELOW_IDENTIFIER_OPT_SPECIAL_CASE = 1002,
    USER = 1003,
    PREC_BELOW_CONTRACTION_TOKEN2 = 1004
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 783 "/home/buildbot/git/sql/sql_yacc.yy" /* yacc.c:1909  */

  int  num;
  ulong ulong_num;
  ulonglong ulonglong_number;
  longlong longlong_number;
  uint sp_instr_addr;

  /* structs */
  LEX_CSTRING lex_str;
  Lex_ident_cli_st kwd;
  Lex_ident_cli_st ident_cli;
  Lex_ident_sys_st ident_sys;
  Lex_string_with_metadata_st lex_string_with_metadata;
  Lex_spblock_st spblock;
  Lex_spblock_handlers_st spblock_handlers;
  Lex_length_and_dec_st Lex_length_and_dec;
  Lex_cast_type_st Lex_cast_type;
  Lex_field_type_st Lex_field_type;
  Lex_dyncol_type_st Lex_dyncol_type;
  Lex_for_loop_st for_loop;
  Lex_for_loop_bounds_st for_loop_bounds;
  Lex_trim_st trim;
  vers_history_point_t vers_history_point;

  /* pointers */
  Create_field *create_field;
  Spvar_definition *spvar_definition;
  Row_definition_list *spvar_definition_list;
  const Type_handler *type_handler;
  CHARSET_INFO *charset;
  Condition_information_item *cond_info_item;
  DYNCALL_CREATE_DEF *dyncol_def;
  Diagnostics_information *diag_info;
  Item *item;
  Item_num *item_num;
  Item_param *item_param;
  Item_basic_constant *item_basic_constant;
  Key_part_spec *key_part;
  LEX *lex;
  sp_assignment_lex *assignment_lex;
  class sp_lex_cursor *sp_cursor_stmt;
  LEX_CSTRING *lex_str_ptr;
  LEX_USER *lex_user;
  List<Condition_information_item> *cond_info_list;
  List<DYNCALL_CREATE_DEF> *dyncol_def_list;
  List<Item> *item_list;
  List<sp_assignment_lex> *sp_assignment_lex_list;
  List<Statement_information_item> *stmt_info_list;
  List<String> *string_list;
  List<LEX_CSTRING> *lex_str_list;
  Statement_information_item *stmt_info_item;
  String *string;
  TABLE_LIST *table_list;
  Table_ident *table;
  Qualified_column_ident *qualified_column_ident;
  char *simple_string;
  const char *const_simple_string;
  chooser_compare_func_creator boolfunc2creator;
  class my_var *myvar;
  class sp_condition_value *spcondvalue;
  class sp_head *sphead;
  class sp_name *spname;
  class sp_variable *spvar;
  class With_element_head *with_element_head;
  class With_clause *with_clause;
  class Virtual_column_info *virtual_column;

  handlerton *db_type;
  st_select_lex *select_lex;
  struct p_elem_val *p_elem_value;
  class Window_frame *window_frame;
  class Window_frame_bound *window_frame_bound;
  udf_func *udf;
  st_trg_execution_order trg_execution_order;

  /* enums */
  enum enum_view_suid view_suid;
  enum sub_select_type unit_type;
  enum Condition_information_item::Name cond_info_item_name;
  enum enum_diag_condition_item_name diag_condition_item_name;
  enum Diagnostics_information::Which_area diag_area;
  enum Field::geometry_type geom_type;
  enum enum_fk_option m_fk_option;
  enum Item_udftype udf_type;
  enum Key::Keytype key_type;
  enum Statement_information_item::Name stmt_info_item_name;
  enum enum_filetype filetype;
  enum enum_tx_isolation tx_isolation;
  enum enum_var_type var_type;
  enum enum_yes_no_unknown m_yes_no_unk;
  enum ha_choice choice;
  enum ha_key_alg key_alg;
  enum ha_rkey_function ha_rkey_mode;
  enum index_hint_type index_hint;
  enum interval_type interval, interval_time_st;
  enum row_type row_type;
  enum sp_variable::enum_mode spvar_mode;
  enum thr_lock_type lock_type;
  enum enum_mysql_timestamp_type date_time_type;
  enum Window_frame_bound::Bound_precedence_type bound_precedence_type;
  enum Window_frame::Frame_units frame_units;
  enum Window_frame::Frame_exclusion frame_exclusion;
  enum trigger_order_type trigger_action_order_type;
  DDL_options_st object_ddl_options;
  enum vers_sys_type_t vers_range_unit;
  enum Column_definition::enum_column_versioning vers_column_versioning;
  enum plsql_cursor_attr_t plsql_cursor_attr;

#line 913 "/home/buildbot/git/mkdist/sql/sql_yacc.hh" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif



int MYSQLparse (THD *thd);

#endif /* !YY_MYSQL_HOME_BUILDBOT_GIT_MKDIST_SQL_SQL_YACC_HH_INCLUDED  */
